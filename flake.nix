{
  description = "A very basic flake";
  inputs.nixpkgs.url = "http://5.75.225.68/313dcfae7e6a0c11f305ba066c83d25c78744333.tar.gz";

  outputs = { self, nixpkgs }:
    let pkgs = import nixpkgs { system = "x86_64-linux"; }; in
    {
      packages.x86_64-linux.default = pkgs.runCommand "hm" {} ''
        ls -la /bin
      '';
    };
}
